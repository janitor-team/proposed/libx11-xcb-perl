libx11-xcb-perl (0.19-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.
  * Remove overrides for lintian tags that are no longer supported.

  [ gregor herrmann ]
  * Import upstream version 0.19.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded alternative (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Oct 2021 01:55:40 +0200

libx11-xcb-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.18.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 10.
  * debian/rules: add --no-parallel.
  * Update renamed lintian override.

 -- gregor herrmann <gregoa@debian.org>  Wed, 10 Oct 2018 20:10:08 +0200

libx11-xcb-perl (0.17-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.17.
    Fixes "FTBFS when debhelper does not export PERL_USE_UNSAFE_INC"
    (Closes: #871800)
  * Drop spelling.patch, applied upstream.
  * Declare compliance with Debian Policy 4.1.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Sep 2017 16:33:57 +0200

libx11-xcb-perl (0.16-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Import upstream version 0.16
  * Drop pod-encoding.patch, included upstream.
  * Declare compliance with Debian Policy 3.9.8.
  * Set bindnow linker flag in debian/rules.
  * Add a patch to fix a typo in the POD.

 -- gregor herrmann <gregoa@debian.org>  Thu, 30 Jun 2016 22:32:40 +0200

libx11-xcb-perl (0.15-2) unstable; urgency=medium

  * Team upload.
  * debian/copyright: add details about upstream copyright/license.
  * Update (build) dependencies. Particularly add libxs-object-magic-perl
    to Depends. (Closes: #810912)

 -- gregor herrmann <gregoa@debian.org>  Wed, 13 Jan 2016 19:39:00 +0100

libx11-xcb-perl (0.15-1) unstable; urgency=low

  * Initial Release. (Closes: #603116)

 -- Michael Stapelberg <stapelberg@debian.org>  Tue, 29 Dec 2015 21:41:19 +0100
